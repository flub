;;;; Operations and definitions for Cantor and Dedekind's set theory.

(defpackage bachelor-cs.set-theory
  (:use :cl)
  (:export :Ø :∈ :⊆ :power :size :∪ :∩ :\\ :Δ :× :→))

(in-package :bachelor-cs.set-theory)

(defvar Ø '()
  "Empty set Ø")

;;; Sets are represented either by a list or a predicate function.

(defgeneric ∈ (element set)
  (:documentation "Predicate to check if ELEMENT is in SET."))

(defmethod ∈ (element (set list))
  (not (null (member element set :test #'equal))))

(defmethod ∈ (element (set function))
  (funcall set element))

(defgeneric ⊆ (set-x set-y)
  (:documentation "Predicate to check if SET-X is a subset of SET-Y.
Implemented only for SET-X represented as a list."))

(defmethod ⊆ ((set-x list) set-y)
  (null (loop for element in set-x
	   unless (∈ element set-y) return t)))

(defmethod ⊆ (set-x (set-y list))
  (null (loop for element in set-y
	   unless (∈ element set-x) return t)))

(defun power (set)
  "Returns the power of SET."
  (lambda (element)
    (or (equal element Ø)
	(⊆ element set))))

(defgeneric size (set)
  (:documentation "Returns cardinality of SET (usually |set|). Only
implemented for SET represented as a list."))

(defmethod size ((set list))
  (length set))

(defmethod size ((set function))
  :∞)

(defgeneric ∪-2 (set-x set-y)
  (:documentation "Union of SET-X and SET-Y."))

(defmethod ∪-2 ((set-x list) (set-y list))
  (remove-duplicates (append set-x set-y)
                     :test #'equal))

(defmethod ∪-2 (set-x set-y)
  (lambda (element)
    (or (∈ element set-x)
	(∈ element set-y))))

(defun ∪ (&rest sets)
  "Union of SETS."
  (reduce #'∪-2 sets))

(defgeneric ∩-2 (set-x set-y)
  (:documentation "Intersection of SET-X and SET-Y."))

(defmethod ∩-2 ((set-x list) (set-y list))
  (intersection set-x set-y :test #'equal))

(defmethod ∩-2 ((set-x list) set-y)
  (loop for element in set-x
     when (∈ element set-y) collect element))

(defmethod ∩-2 (set-x (set-y list))
  (loop for element in set-y
     when (∈ element set-x) collect element))

(defmethod ∩-2 (set-x set-y)
  (lambda (element)
    (and (∈ element set-x)
	 (∈ element set-y))))

(defun ∩ (&rest sets)
  "Intersection of SETS."
  (reduce #'∩-2 sets))

(defgeneric \\-2 (set-x set-y)
  (:documentation "Subtracts SET-Y from SET-X."))

(defmethod \\-2 ((set-x list) set-y)
  (remove-if (lambda (element) (∈ element set-y))
	     set-x))

(defmethod \\-2 (set-x (set-y list))
  (remove-if (lambda (element) (∈ element set-x))
	     set-y))

(defmethod \\-2 (set-x set-y)
  (lambda (element)
    (and (∈ element set-x)
	 (not (∈ element set-y)))))

(defun \\ (&rest sets)
  "Subtracts SETS."
  (reduce #'\\-2 sets))

(defun Δ-2 (set-x set-y)
  "Symmetric difference for SET-X and SET-Y."
  (∪ (\\ set-x set-y) (\\ set-y set-x)))

(defun Δ (&rest sets)
  "Symmetric difference for SETS."
  (reduce #'Δ-2 sets))

(defgeneric ×-2 (set-x set-y)
  (:documentation "Cartesian product of SET-X and SET-Y."))

(defmethod ×-2 ((set-x list) (set-y list))
  (remove-duplicates
   (loop for element-x in set-x append
	(loop for element-y in set-y collect
	     (list element-x element-y)))
   :test #'equal))
  
(defmethod ×-2 (set-x set-y)
  (lambda (element)
    (and (∈ (first element) set-x)
	 (∈ (second element) set-y))))

(defun × (&rest sets)
  "Cartesian product of SETS."
  (remove-duplicates
   (labels ((tuple (product)
              (dotimes (i (- (length sets) 2))
                (setf product (append (car product)
                                      (cdr product))))
              product))
     (loop for product in (reduce #'×-2 sets)
        collect (tuple product)))
   :test #'equal))

(defmacro → (args &rest body)
  "Return a function that takes one argument, destructures it accoring to
ARGS and then evaluates BODY."
  (let ((g!element (gensym "element")))
    `(lambda (,g!element)
       (destructuring-bind ,args ,g!element
         ,@body))))
