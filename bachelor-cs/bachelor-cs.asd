;;;; System definition for bachelor-cs.

(defpackage bachelor-cs-asd
  (:use :cl :asdf))

(in-package :bachelor-cs-asd)

(defsystem bachelor-cs
  :components ((:file "set-theory")))
